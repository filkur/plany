<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $guarded = [];

    public function lecturers()
	{
	    return $this->belongsToMany('App\Lecturer');
	}

	public function groups()
	{
	    return $this->belongsToMany('App\Group');
	}
}
