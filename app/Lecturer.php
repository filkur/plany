<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lecturer extends Model
{
    protected $guarded = [];

    public function subjects()
    {
    	return $this->belongsToMany('App\Subject');
    }

    public function getLatestSubjects()
    {
    	return $this->subjects()->latest()->get();
    }
}
