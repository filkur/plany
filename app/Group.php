<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $guarded = [];

    public function study()
    {
    	return $this->belongsTo('App\Study');
    }

    public function subjects()
    {
    	return $this->belongsToMany('App\Subject');
    }
}
