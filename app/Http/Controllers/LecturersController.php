<?php

namespace App\Http\Controllers;

use App\Lecturer;
use App\Subject;
use Illuminate\Http\Request;

class LecturersController extends Controller
{
    public function index(Request $request)
    {
        $lecturers = Lecturer::with('subjects')->get();

        if($request->expectsJson()){
            return $lecturers->toJson();
        }

    	return view('lecturers.index', compact('lecturers'));
    }

    public function show(Lecturer $lecturer)
    {
        $subject_list = Subject::whereNotIn('id', $lecturer->subjects->pluck('id'))->get();

        $subjects = $lecturer->getLatestSubjects();

        return view('lecturers.show', compact('lecturer', 'subject_list', 'subjects'));
    }

    public function assignSubject(Request $request, Lecturer $lecturer)
    {  
        $lecturer->subjects()->attach($request->subject);

        return redirect('/lecturers/' . $lecturer->id)
            ->with('flash', 'Przedmiot został przypisany');
    }

    public function removeSubject(Lecturer $lecturer, Subject $subject)
    {
        $lecturer->subjects()->detach($subject);

        return redirect('/lecturers/' . $lecturer->id)
            ->with('flash', 'Przedmiot usunięty');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'name' => 'required|alpha',
            'surname' => 'required|alpha'
        ]);

		$lecturer = Lecturer::create([
			'title' => request('title'),
			'name' => request('name'),
			'surname' => request('surname')
		]);

		return redirect('/lecturers')
            ->with('flash', 'Wykładowca został dodany');
    }

    public function update(Request $request, Lecturer $lecturer)
    {

        $this->validate($request, [
            'title' => 'required',
            'name' => 'required|alpha',
            'surname' => 'required|alpha'
        ]);
        
        $lecturer->title = $request->title;
        $lecturer->name = $request->name;
        $lecturer->surname = $request->surname;

        $lecturer->save();
        
        return redirect('/lecturers/' . $lecturer->id)
            ->with('flash', 'Dane zostały zaktualizowane');
    }

    public function destroy(Lecturer $lecturer)
    {
        $lecturer->delete();

        return redirect('/lecturers')
            ->with('flash', 'Wykładowca został usunięty');
    }
}

