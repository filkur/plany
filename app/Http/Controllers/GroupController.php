<?php

namespace App\Http\Controllers;

use App\Group;
use App\Study;
use App\Subject;
use Illuminate\Validation\Rule;

use Illuminate\Http\Request;

class GroupController extends Controller
{
    public function index(Request $request)
    {
        $groups = Group::all();
        $studies = Study::all();

        // $queries = $request->query;

        // foreach($queries as $query => $val){
        //     $groups = Group::where($query, $val)->get();
        // }

        if($request->expectsJson()){
            return $groups->toJson();
        }

        return view('groups.index', compact('groups','studies'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'number' => 'required|numeric|unique:groups,number,NULL,NULL,semester,'.$request->semester.',study_id,'.$request->study_id,
            'semester' => 'required|numeric',
            'number_of_students' => 'required|numeric',
            'study_id' => 'required'
        ],
        [
            'number.unique' => 'Grupa już istnieje',
        ]
    );

        $group = Group::create([
            'number' => request('number'),
            'semester' => request('semester'),
            'number_of_students' => request('number_of_students'),
            'study_id' => request('study_id')
        ]);

        return redirect('/groups')
            ->with('flash', 'Grupa została dodana');
    }

    public function destroy(Group $group)
    {
        $group->delete();

        return redirect('/groups')
            ->with('flash', 'Grupa została usunięta');
    }

    public function show(Group $group)
    {
        $subject_list = Subject::whereNotIn('id', $group->subjects->pluck('id'))->get();

        $subjects = $group->subjects;

        $studies = Study::all();

        return view('groups.show', compact('group', 'subject_list', 'subjects', 'studies'));
    }

    public function update(Request $request, Group $group)
    {

        $this->validate($request, [
            'number' => 'required|integer',
            'semester' => 'required|integer',
            'number_of_students' => 'required|integer',
            'study_id' => 'required|integer'
        ]);
        
        $group->number = $request->number;
        $group->semester = $request->semester;
        $group->number_of_students = $request->number_of_students;
        $group->study_id = $request->study_id;

        $group->save();
        
        return redirect('/groups/' . $group->id)
            ->with('flash', 'Dane zostały zaktualizowane');
    }

    public function assignSubject(Request $request, Group $group)
    {  
        $group->subjects()->attach($request->subject);

        return redirect('/groups/' . $group->id)
            ->with('flash', 'Przedmiot został przypisany');
    }

    public function removeSubject(Group $group, Subject $subject)
    {
        $group->subjects()->detach($subject);

        return redirect('/groups/' . $group->id)
            ->with('flash', 'Przedmiot usunięty');
    }
}
