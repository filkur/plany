<?php

namespace App\Http\Controllers;

use App\Classroom;
use Illuminate\Http\Request;

class ClassroomController extends Controller
{
    public function index(Request $request)
    {
        $classrooms = Classroom::all();

        if($request->expectsJson()){
            return $classrooms->toJson();
        }

        return view('classrooms.index', compact('classrooms'));
    }
    
    public function store(Request $request)
    {
        $this->validate($request, [
            'number' => 'required|numeric|unique:classrooms',
            'capacity' => 'required|numeric'
        ]);

        $subject = Classroom::create([
            'number' => request('number'),
            'capacity' => request('capacity')
        ]);

        return redirect('/classrooms')
            ->with('flash', 'Sala została dodana');
    }

    public function destroy(Classroom $classroom)
    {
        $classroom->delete();

        return redirect('/classrooms')
            ->with('flash', 'Sala została usunięta');
    }
}
