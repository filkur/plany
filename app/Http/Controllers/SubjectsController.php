<?php

namespace App\Http\Controllers;

use App\Subject;
use Illuminate\Http\Request;

class SubjectsController extends Controller
{
    public function index(Request $request)
    {
    	$subjects = Subject::all();

        if($request->expectsJson()){
            return $subjects->toJson();
        }

    	return view('subjects.index', compact('subjects'));
    }

    public function store(Request $request)
    {
    	$this->validate($request, [
    		'name' => 'required|unique:subjects'
    	]);

    	$subject = Subject::create([
    		'name' => request('name')
    	]);

    	return redirect('/subjects')
            ->with('flash', 'Przedmiot został dodany');
    }

    public function destroy(Subject $subject)
    {
    	$subject->delete();

        return redirect('/subjects')
            ->with('flash', 'Przedmiot został usunięty');
    }
}
