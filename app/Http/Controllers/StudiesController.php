<?php

namespace App\Http\Controllers;

use App\Study;
use Illuminate\Http\Request;

class StudiesController extends Controller
{
    public function index()
    {
        $studies = Study::all();

        return view('studies.index', compact('studies'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:studies'
        ]);

        $study = Study::create([
            'name' => request('name')
        ]);

        return redirect('/studies')
            ->with('flash', 'Kierunek został dodany');
    }

    public function show(Study $study)
    {
        //
    }

    public function edit(Study $study)
    {
        //
    }

    public function update(Request $request, Study $study)
    {
        //
    }

    public function destroy(Study $study)
    {
        $study->delete();

        return redirect('/studies')
            ->with('flash', 'Kierunek został usunięty');
    }
}
