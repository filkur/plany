<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Plan extends Model
{
    public function getSemesters()
    {
    	return DB::table('groups')->distinct()->get(['semester']);
    }

    public function getDays()
    {
    	return DB::table('days')->get();
    }

    public function getHours()
    {
    	return DB::table('hours')->get();
    }
}
