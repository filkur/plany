<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('groups', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('number');
            $table->integer('semester');
            $table->integer('number_of_students');
            $table->unsignedInteger('study_id');
            $table->foreign('study_id')->references('id')->on('studies');
            $table->unique(['number','semester','study_id']);
            $table->timestamps();
        });

        Schema::create('group_subject', function (Blueprint $table) {
            $table->integer('group_id');
            $table->integer('subject_id');
            $table->primary(['group_id', 'subject_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('groups');
        Schema::dropIfExists('group_subject');
    }
}
