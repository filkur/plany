@extends('layouts.app')

@section('content')
<plan-form :groups="{{ $groups }}" :studies="{{ $studies }}" inline-template>
	<div class="container">

		<div class="form-row">

			<div class="form group col-md-4">
				<label for="study">Kierunek</label>
				<select type="select" class="form-control" name="study" id="study" v-model="selectedStudyId" @change="getSemesters()">
					<option value="">Wybierz</option>

					<option v-for="study in studies" v-bind:value="study.id">@{{study.name}}</option>

				</select>
			</div>     

			<div class="form group col-md-4" v-if="semesterVisible">
				<label for="group">Semestr</label>
				<select type="select" class="form-control" name="semester" id="semester" v-model="selectedSemester" @change="getGroups()">
					<option value="">Wybierz</option>

					<option v-for="semester in semesters" v-bind:value="semester">@{{ semester }}</option>

				</select>
			</div>

			<div class="form group col-md-4" v-if="groupVisible">
				<label for="group">Grupa</label>
				<select type="select" class="form-control" name="group" id="group" v-model="selectedGroupId" @change="showForm()">
					<option value="">Wybierz</option>

					<option v-for="group in filteredGroups" v-bind:value="group.id">@{{ group.number }}</option>
				</select>
			</div>

			<div class="container" v-if="formVisible">
				<div class="row plan-form">

					<div class="col-md-2">

						<div class="row cell">
							<strong>#</strong>
						</div>

						<div class="row cell" v-for="hour in hours" v-bind:id="hour.id">
							<strong>@{{ hour.time }}</strong>
						</div>

					</div>

					<div class="col-md-2" v-for="day in days" v-bind:id="day.id" :key="day.id">

						<div class="row cell">
							<strong>@{{ day.name }}</strong>
						</div>

						<plan-cell class="row cell cell-hover" v-for="hour in hours" :dayid="day.id" :hourid="hour.id" :subjects="subjects" :classrooms="classrooms" :key="this.id"></plan-cell>
					</div>

				</div>	
			</div>
		</div>
	</div>
</plan-form>
@endsection