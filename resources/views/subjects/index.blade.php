@extends('layouts.app')

@section('content')
	<div class="container">
		<h2>Zajęcia</h2>

		@include('partials.errors')

		<form action="/subjects" method="POST">
			{{ csrf_field() }}
		  <div class="form-row">
		    <div class="form-group col-md-6">
		      <label for="name">Nazwa</label>
		      <input type="text" class="form-control" name="name" id="name" placeholder="Nazwa" required>
		    </div>
		  </div>
		  <button type="submit" class="btn btn-primary">Dodaj</button>
		</form>
	</div>
	
	<div class="container">
		<table class="table">
			<thead class="bg-secondary text-white">
				<tr>
					<th>ID</th>
					<th>Nazwa</th>
					<th>Akcja</th>
				</tr>
			</thead>
			<tbody>
				@foreach($subjects as $subject)
					<tr>
						<td>{{ $subject->id }}</td>
						<td>{{ $subject->name }}</td>
						<td>
							<form action="/subjects/{{ $subject->id }}" method="POST">
								{{ csrf_field() }}
								{{ method_field('DELETE') }}
								<button class="btn btn-danger">X</button>
							</form>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
@endsection