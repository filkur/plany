@extends('layouts.app')

@section('content')
	<div class="container">
		<h2>Sale</h2>

		@include('partials.errors')
		
		<form action="/classrooms" method="POST">
			{{ csrf_field() }}
		  <div class="form-row">
		    <div class="form-group col-md-6">
		      <label for="number">Numer</label>
		      <input type="text" class="form-control" name="number" id="number" placeholder="Numer" required>
		    </div>
		    <div class="form-group col-md-6">
		      <label for="capacity">Miejsca</label>
		      <input type="text" class="form-control" name="capacity" id="capacity" placeholder="Miejsca" required>
		    </div>
		  </div>
		  <button type="submit" class="btn btn-primary">Dodaj</button>
		</form>
	</div>
	<div class="container">
		<table class="table">
			<thead class="bg-secondary text-white">
				<tr>
					<th>ID</th>
					<th>Numer</th>
					<th>Miejsca</th>
					<th>Akcja</th>
				</tr>
			</thead>
			<tbody>
				@foreach($classrooms as $classroom)
					<tr>
						<td>{{ $classroom->id }}</td>
						<td>{{ $classroom->number }}</td>
						<td>{{ $classroom->capacity }}</td>
						<td>
							<form action="/classrooms/{{ $classroom->id }}" method="POST">
								{{ csrf_field() }}
								{{ method_field('DELETE') }}
								<button class="btn btn-danger">X</button>
							</form>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
@endsection