<nav class="navbar navbar-expand-lg navbar-dark bg-secondary">
  <div class="container">
    <div class="navbar-header">
      <a class="navbar-brand" href="{{ url('/') }}">
        {{ config('app.name', 'Plany') }}
      </a>
    </div>

    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" href="{{ route('lecturers') }}">Wykładowcy</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{ route('subjects') }}">Przedmioty</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{ route('groups') }}">Grupy</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{ route('studies') }}">Kierunki</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{ route('classrooms') }}">Sale</a>
        </li>
      </ul>
    </div>
    <!-- Right Side Of Navbar -->
    <ul class="nav navbar-nav navbar-right">
    <!-- Authentication Links -->
      @guest
        <li class="nav-item active"><a class="nav-link" href="{{ route('login') }}">Login</a></li>
        <li class="nav-item active"><a class="nav-link" href="{{ route('register') }}">Register</a></li>
      @else
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
            {{ Auth::user()->name }} <span class="caret"></span>
          </a>

          <ul class="dropdown-menu" role="menu">
            <li>
              <a href="{{ route('profile', Auth::user()) }}">My Profile</a>
            </li>
            <li>
              <a href="{{ route('logout') }}"
                onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
                Logout
              </a>

              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
              </form>
            </li>
          </ul>
        </li>
      @endguest
    </ul>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
  </div>

</nav>