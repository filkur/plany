@extends('layouts.app')

@section('content')
	<div class="container">
		<h2>{{ $lecturer->title }} {{ $lecturer->name }} {{ $lecturer->surname }}</h2>

		@include('partials.errors')
		
		<form action="/lecturers/{{ $lecturer->id }}" method="POST">
			{{ csrf_field() }}
			{{ method_field('PATCH') }}
		  <div class="form-row">
		    <div class="form-group col-md-1">
		      <label for="title">Tytuł</label>
		      <input type="text" class="form-control" name="title" id="title" placeholder="Tytuł" value="{{ $lecturer->title }}">
		    </div>
		    <div class="form-group col-md-5">
		      <label for="name">Imię</label>
		      <input type="text" class="form-control" name="name" id="name" placeholder="Imię" value="{{ $lecturer->name }}">
		    </div>
		    <div class="form-group col-md-6">
		      <label for="surname">Nazwisko</label>
		      <input type="text" class="form-control" name="surname" id="surname" placeholder="Nazwisko" value="{{ $lecturer->surname }}">
		    </div>
		  </div>
		  <button type="submit" class="btn btn-primary">Zapisz</button>
		</form>
	</div>
	<div class="container">
		<h3>Przypisz przedmioty</h3>
		<form action="/lecturers/{{ $lecturer->id }}/assign_subject" method="POST">
			{{ csrf_field() }}
			<div class="form-row">
			    <div class="form-group col-md-6">
			      <label for="subject">Nazwa</label>
			      <select type="select" class="form-control" name="subject" id="subject" placeholder="Subject">
			      	<option value="">Wybierz...</option>
			      	@foreach($subject_list as $item)
			      		<option value="{{ $item->id }}">{{ $item->name }}</option>
			      	@endforeach
			      </select>
			    </div>
			</div>
			<button type="submit" class="btn btn-primary">Przypisz</button>
		</form>
	</div>
	<div class="container">
		<table class="table">
			<thead class="bg-secondary text-white">
				<tr>
					<th>ID</th>
					<th>Akcja</th>
				</tr>
			</thead>
			<tbody>
				@foreach($subjects as $subject)
					<tr>
						<td>{{ $subject->name }}</td>
						<td>
							<form class="d-inline-block" action="/lecturers/{{ $lecturer->id }}/{{ $subject->id }}" method="POST">
								{{ csrf_field() }}
								{{ method_field('DELETE') }}
								<button class="btn btn-danger">X</button>
							</form>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
@endsection