@extends('layouts.app')

@section('content')
	<div class="container">
		<h2>Wykładowcy</h2>

		@include('partials.errors')
		
		<form action="/lecturers" method="POST">
			{{ csrf_field() }}
		  <div class="form-row">
		    <div class="form-group col-md-1">
		      <label for="title">Tytuł</label>
		      <input type="text" class="form-control" name="title" id="title" placeholder="Tytuł" required>
		    </div>
		    <div class="form-group col-md-5">
		      <label for="name">Imię</label>
		      <input type="text" class="form-control" name="name" id="name" placeholder="Imię" required>
		    </div>
		    <div class="form-group col-md-6">
		      <label for="surname">Nazwisko</label>
		      <input type="text" class="form-control" name="surname" id="surname" placeholder="Nazwisko" required>
		    </div>
		  </div>
		  <button type="submit" class="btn btn-primary">Dodaj</button>
		</form>
	</div>
	<div class="container">
		<table class="table">
			<thead class="bg-secondary text-white">
				<tr>
					<th>ID</th>
					<th>Tytuł</th>
					<th>Imię</th>
					<th>Nazwisko</th>
					<th>Akcja</th>
				</tr>
			</thead>
			<tbody>
				@foreach($lecturers as $lecturer)
					<tr>
						<td>{{ $lecturer->id }}</td>
						<td>{{ $lecturer->title }}</td>
						<td>{{ $lecturer->name }}</td>
						<td>{{ $lecturer->surname }}</td>
						<td>
							<form class="d-inline-block" action="/lecturers/{{ $lecturer->id }}" method="POST">
								{{ csrf_field() }}
								{{ method_field('DELETE') }}
								<button class="btn btn-danger">X</button>
							</form>
							<form class="d-inline-block" action="/lecturers/{{ $lecturer->id }}" method="GET">
								<button class="btn btn-primary">Edycja</button>
							</form>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
@endsection