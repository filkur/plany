@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">

                <h3>Przeglądaj</h3>
                <a href="/plans">Przeglądaj</a>

                <h3>Dodaj plan</h3>
                <a href="/plans/add" class="text">Dodaj</a>

            </div>
        </div>
    </div>
</div>
@endsection
