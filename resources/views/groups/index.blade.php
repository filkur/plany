@extends('layouts.app')

@section('content')
	<div class="container">
		<h2>Grupy</h2>

		@include('partials.errors')

		<form action="/groups" method="POST">
			{{ csrf_field() }}
		  <div class="form-row">
		    <div class="form-group col-md-4">
		      <label for="number">Numer</label>
		      <input type="text" class="form-control" name="number" id="number" placeholder="Numer" value="{{ old('number') }}" required>
		    </div>
		    <div class="form-group col-md-4">
		      <label for="semester">Semestr</label>
		      <input type="text" class="form-control" name="semester" id="semester" placeholder="Semestr" value="{{ old('semester') }}" required>
		    </div>
		    <div class="form-group col-md-4">
		      <label for="number_of_students">Liczba uczniów</label>
		      <input type="text" class="form-control" name="number_of_students" id="number_of_students" placeholder="Liczba uczniów" value="{{ old('number_of_students') }}" required>
		    </div>
		  </div>
		  <div class="form-row">
		  	<div class="form-group col-md-12">
		  	  <label for="study_id">Kierunek</label>
		  	  <select type="select" name="study_id" id="study_id" placeholder="Kierunek" class="form-control" value="{{ old('study_id') }}">
		  	  		<option value="" selected>Wybierz...</option>
			  	  	@foreach($studies as $study)
			  	  	<option value="{{ $study->id }}">{{ $study->name }}</option>
			  	  	@endforeach
		  	  </select>
		  	</div>
		  </div>
		  <button type="submit" class="btn btn-primary">Dodaj</button>
		</form>
	</div>
	<div class="container">
		<table class="table">
			<thead class="bg-secondary text-white">
				<tr>
					<th>ID</th>
					<th>Numer</th>
					<th>Semestr</th>
					<th>Liczba uczniów</th>
					<th>Kierunek</th>
					<th>Akcja</th>

				</tr>
			</thead>
			<tbody>
				@foreach($groups as $group)
					<tr>
						<td>{{ $group->id }}</td>
						<td>{{ $group->number }}</td>
						<td>{{ $group->semester }}</td>
						<td>{{ $group->number_of_students }}</td>
						<td>{{ $group->study->name }}</td>
						<td>
							<form class="d-inline-block" action="/groups/{{ $group->id }}" method="POST">
								{{ csrf_field() }}
								{{ method_field('DELETE') }}
								<button class="btn btn-danger">X</button>
							</form>
							<form class="d-inline-block" action="/groups/{{ $group->id }}" method="GET">
								<button class="btn btn-primary">Edycja</button>
							</form>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
@endsection