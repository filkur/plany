@extends('layouts.app')

@section('content')
	<div class="container">
		<h2>{{ $group->title }} {{ $group->name }} {{ $group->surname }}</h2>

		@include('partials.errors')
		
		<form action="/groups/{{ $group->id }}" method="POST">
			{{ csrf_field() }}
			{{ method_field('PATCH') }}
		  <div class="form-row">
		    <div class="form-group col-md-4">
		      <label for="number">Numer</label>
		      <input type="text" class="form-control" name="number" id="number" placeholder="Numer" value="{{ $group->number }}" required>
		    </div>
		    <div class="form-group col-md-4">
		      <label for="semester">Semestr</label>
		      <input type="text" class="form-control" name="semester" id="semester" placeholder="Semestr" value="{{ $group->semester }}" required>
		    </div>
		    <div class="form-group col-md-4">
		      <label for="number_of_students">Liczba uczniów</label>
		      <input type="text" class="form-control" name="number_of_students" id="number_of_students" placeholder="Liczba uczniów" value="{{ $group->number_of_students }}" required>
		    </div>
		  </div>
		  <div class="form-row">
		  	<div class="form-group col-md-12">
		  	  <label for="study_id">Kierunek</label>
		  	  <select type="select" name="study_id" id="study_id" placeholder="Kierunek" class="form-control" value="{{ $group->study_id }}">
		  	  		<option value="">Wybierz...</option>
			  	  	@foreach($studies as $study)
			  	  	<option value="{{ $study->id }}" @if ($study->id == $group->study_id) selected @endif>{{ $study->name }}</option>
			  	  	@endforeach
		  	  </select>
		  	</div>
		  </div>
		  <button type="submit" class="btn btn-primary">Zapisz zmiany</button>
		</form>
	</div>
	<div class="container">
		<h3>Przypisz przedmioty</h3>
		<form action="/groups/{{ $group->id }}/assign_subject" method="POST">
			{{ csrf_field() }}
			<div class="form-row">
			    <div class="form-group col-md-6">
			      <label for="subject">Nazwa</label>
			      <select type="select" class="form-control" name="subject" id="subject" placeholder="Subject">
			      	<option value="">Wybierz...</option>
			      	@foreach($subject_list as $item)
			      		<option value="{{ $item->id }}">{{ $item->name }}</option>
			      	@endforeach
			      </select>
			    </div>
			</div>
			<button type="submit" class="btn btn-primary">Przypisz</button>
		</form>
	</div>
	<div class="container">
		<table class="table">
			<thead class="bg-secondary text-white">
				<tr>
					<th>ID</th>
					<th>Akcja</th>
				</tr>
			</thead>
			<tbody>
				@foreach($subjects as $subject)
					<tr>
						<td>{{ $subject->name }}</td>
						<td>
							<form class="d-inline-block" action="/groups/{{ $group->id }}/{{ $subject->id }}" method="POST">
								{{ csrf_field() }}
								{{ method_field('DELETE') }}
								<button class="btn btn-danger">X</button>
							</form>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
@endsection