<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/profile')->name('profile');
Route::get('/', 'HomeController@index')->name('home');

Route::get('/lecturers', 'LecturersController@index')->name('lecturers');
Route::post('/lecturers', 'LecturersController@store');
Route::delete('/lecturers/{lecturer}', 'LecturersController@destroy');
Route::patch('/lecturers/{lecturer}', 'LecturersController@update');
Route::get('/lecturers/{lecturer}', 'LecturersController@show');
Route::post('/lecturers/{lecturer}/assign_subject', 'LecturersController@assignSubject');
Route::delete('/lecturers/{lecturer}/{subject}', 'LecturersController@removeSubject');

Route::get('/subjects', 'SubjectsController@index')->name('subjects');
Route::post('/subjects', 'SubjectsController@store');
Route::delete('/subjects/{subject}', 'SubjectsController@destroy');

Route::get('/groups', 'GroupController@index')->name('groups');
Route::post('/groups', 'GroupController@store');
Route::get('/groups/{group}', 'GroupController@show');
Route::patch('/groups/{group}', 'GroupController@update');
Route::delete('/groups/{group}', 'GroupController@destroy');
Route::post('/groups/{group}/assign_subject', 'GroupController@assignSubject');
Route::delete('/groups/{group}/{subject}', 'GroupController@removeSubject');

Route::get('/studies', 'StudiesController@index')->name('studies');
Route::post('/studies', 'StudiesController@store');
Route::delete('/studies/{study}', 'StudiesController@destroy');

Route::get('/classrooms', 'ClassroomController@index')->name('classrooms');
Route::post('/classrooms', 'ClassroomController@store');
Route::delete('/classrooms/{classroom}', 'ClassroomController@destroy');

Route::get('/plans', 'PlanController@index')->name('plans');
Route::get('/plans/add', 'PlanController@create');
Route::post('/plans/add', 'PlanController@store');
Route::post('/plans', 'PlanController@store');

Route::post('/days/get', 'PlanController@days');
Route::post('/hours/get', 'PlanController@hours');